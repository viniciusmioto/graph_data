import json

file = "emb.txt"
embedding = open('DATABASE_embedding.txt',"w")

label = ""

with open(file, "r") as read_file:
	data = json.load(read_file)

	for i in range(1,135):
		graph = '../graphs/'+str(i)+'.gexf.g2v3'
		array = data[graph]

		embedding.write(label+" ")

		j = 1
		for v in array:
			embedding.write(str(j)+':'+str(v)+" ")
			j += 1

		embedding.write("\n")


embedding.close